-- Aufgabe:		Auswahl aller Rezepte einer bestimmten Ernährungskategorie
-- Anforderung:	Inner Join
SELECT REZ_ID AS 'Rezeptnummer', REZEPT.BEZEICHNUNG AS 'Rezeptname', ERNAEHRUNGSKATEGORIE.BEZEICHNUNG AS 'ERNÄHRUNGSKATEGORIE'
FROM REZEPT
INNER JOIN ERNAEHRUNGSKATEGORIE 				-- Inner Join
ON REZEPT.ERN_ID = ERNAEHRUNGSKATEGORIE.ERN_ID
WHERE ERNAEHRUNGSKATEGORIE.ERN_ID = '2';


-- Aufgabe:		Auswahl bzw. Ausschluss von Rezepten auf Basis von Beschränkungen
-- Anforderung:	Left Join
SELECT  REZEPT.REZ_ID AS 'Rezeptnummer', REZEPT.Bezeichnung AS 'Rezeptname'
FROM REZEPT
LEFT JOIN REZEPT_BESCHRAENKUNG_ht				-- Left Join
ON REZEPT.REZ_ID = REZEPT_BESCHRAENKUNG_ht.REZ_ID
LEFT JOIN BESCHRAENKUNG							-- Left Join
ON REZEPT_BESCHRAENKUNG_ht.BES_ID = BESCHRAENKUNG.BES_ID
-- NOT wegnehmen für Auswahl von Beschränkungen (anstatt Ausschluss)
WHERE not BESCHRAENKUNG.BES_ID = '2'			-- < Hier Beschränkungs-ID eintragen!
GROUP BY REZEPT.bezeichnung;


-- Aufgabe: 	Zusammenstellung von Zutaten entsprechend eines Rezepts
-- Anforderung: Inner Join
SELECT ZUTAT.BEZEICHNUNG AS 'Zutat', Round(REZEPT_ZUTAT_ht.MENGE * ZUTAT.Faktor, +2) AS 'Menge', ZUTAT.EINHEIT AS 'Einheit'
FROM REZEPT_ZUTAT_ht
INNER JOIN ZUTAT								-- Inner Join
ON ZUTAT.ZUT_ID = REZEPT_ZUTAT_ht.ZUT_ID
INNER JOIN REZEPT								-- Inner Join
ON REZEPT_ZUTAT_ht.REZ_ID = REZEPT.REZ_ID
WHERE REZEPT_ZUTAT_ht.REZ_ID = '1';


-- Aufgabe:		Auswahl aller Zutaten eines Rezeptes
-- Anforderung: Inner Join
SELECT ZUTAT.BEZEICHNUNG AS 'Zutaten für Lachslasagne'
FROM ZUTAT
INNER JOIN REZEPT_ZUTAT_ht 						-- Inner Join
ON ZUTAT.ZUT_ID = REZEPT_ZUTAT_ht.ZUT_ID 
INNER JOIN REZEPT 								-- Inner Join
ON REZEPT.REZ_ID = REZEPT_ZUTAT_ht.REZ_ID
WHERE REZEPT.BEZEICHNUNG LIKE 'Lachslasagne';	-- < Am Beispiel der Lachslasagne


-- Aufgabe:		Auswahl aller Zutaten, die bisher keinem Rezept zugeordnet sind
-- Anforderung: Subselect, Right Join
SELECT ZUTAT.BEZEICHNUNG AS 'Zutat'
FROM ZUTAT
WHERE ZUT_ID IN ( 								-- Subselect
	SELECT B.ZUT_ID 
	FROM REZEPT_ZUTAT_ht AS A 					-- Alias A
	RIGHT JOIN ZUTAT AS B 						-- Alias B; Right Join
	ON A.ZUT_ID = B.ZUT_ID 
	WHERE A.ZUT_ID IS NULL
	);


-- Aufgabe:		Auswahl aller Rezepte, die eine gewisse Zutat enthalten
-- Anforderung: Inner Join
SELECT REZEPT.BEZEICHNUNG AS 'Rezept', ZUTAT.BEZEICHNUNG AS 'enthält Zutat'
FROM REZEPT
INNER JOIN REZEPT_ZUTAT_ht 						-- Inner Join
ON REZEPT.REZ_ID = REZEPT_ZUTAT_ht.REZ_ID 
INNER JOIN ZUTAT 								-- Inner Join
ON REZEPT_ZUTAT_ht.ZUT_ID = ZUTAT.ZUT_ID 
WHERE ZUTAT.BEZEICHNUNG LIKE '$Zutat'; 			-- < Hier Zutat eintragen!


-- Aufgabe:		Berechnung der durchschnittlichen Nährwerte aller Bestellungen eines Kunden
-- Anforderung: Inner Join
SELECT K.VORNAME AS 'Vorname', K.NACHNAME AS 'Nachname', B.BES_ID AS 'Bestell-ID', SUM(Z.KALORIEN * RZht.MENGE) / COUNT(B.BES_ID) AS 'Ø Kalorien / Bestellung', SUM(Z.KOHLENHYDRATE * RZht.MENGE) / COUNT(B.BES_ID) AS 'Ø Kohlenhydrate / Bestellung', SUM(Z.PROTEIN * RZht.MENGE) / COUNT(B.BES_ID) AS 'Ø Proteine / Bestellung'
FROM ZUTAT AS Z									-- Alias Z
INNER JOIN REZEPT_ZUTAT_ht AS RZht 				-- Alias RZht; Inner Join 
ON Z.ZUT_ID = RZht.ZUT_ID 
INNER JOIN BESTELLUNG_REZEPT_ht AS BRht 		-- Alias BRht; Inner Join 
ON RZht.REZ_ID = BRht.REZ_ID 
INNER JOIN BESTELLUNG AS B 						-- Alias B; Inner Join 
ON B.BES_ID = BRht.BES_ID 
INNER JOIN KUNDE AS K 							-- Alias K; Inner Join 
ON K.KD_ID = B.KD_ID
WHERE K.KD_ID LIKE '$KundenID' 					-- < Hier KundenID eintragen
-- WHERE K.NACHNAME LIKE 'Wellensteyn' AND K.VORNAME LIKE 'Kira'


-- Aufgabe:		Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten
-- Anforderung: Aggregatfunktion, Inner Join
-- optional
SELECT REZEPT.BEZEICHNUNG AS 'Rezeptname'
FROM REZEPT
INNER JOIN REZEPT_ZUTAT_ht 						-- Inner Join
ON REZEPT_ZUTAT_ht.REZ_ID = REZEPT.REZ_ID
GROUP BY REZEPT.REZ_ID
HAVING COUNT(REZEPT_ZUTAT_ht.ZUT_ID) <= 5; 		-- Aggregatfunktion: Count()


-- Aufgabe:		Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten und eine bestimmte Ernährungskategorie erfüllen
-- Anforderung: Aggregatfunktion, Inner Join
-- optional
SELECT DISTINCT R.BEZEICHNUNG AS 'Rezeptname'
FROM REZEPT AS R 								-- Alias R
INNER JOIN REZEPT_ZUTAT_ht AS RZht 				-- Alias RZht; Inner Join
ON RZht.REZ_ID = R.REZ_ID
INNER JOIN ERNAEHRUNGSKATEGORIE AS E 			-- Alias E; Inner Join
ON E.ERN_ID = R.ERN_ID 
WHERE E.BEZEICHNUNG LIKE '$Ernährungskategorie' -- < Hier Ernährungskategorie eintragen
GROUP BY R.REZ_ID
HAVING COUNT(RZht.ZUT_ID) <= 5;					-- Aggregatfunktion: Count()


-- Aufgabe:		Auswahl aller Rezepte, die eine bestimmte Kalorienmenge nicht überschreiten
-- Anforderung: Aggregatfunktion, Inner Join
-- optional
SELECT R.BEZEICHNUNG AS 'Rezeptname',
SUM(Z.KALORIEN * RZht.MENGE) AS 'Summe Kalorien'
FROM REZEPT AS R								-- Alias R
INNER JOIN REZEPT_ZUTAT_ht AS RZht				-- Alias RZht; Inner Join
ON RZht.REZ_ID = R.REZ_ID 
INNER JOIN ZUTAT AS Z							-- Alias Z; Inner Join
ON Z.ZUT_ID = RZht.ZUT_ID 
GROUP BY R.REZ_ID 
HAVING SUM(Z.KALORIEN * RZht.MENGE) <= '$KG' 	-- < Hier Kaloriengrenze eintragen; Aggregatfunktion: SUM()
