CREATE TABLE BESTELLUNG_REZEPT_ht (
    REZ_ID int,
    BES_ID int,
    MENGE float,
    FOREIGN KEY (REZ_ID) REFERENCES REZEPT(REZ_ID),
    FOREIGN KEY (BES_ID) REFERENCES BESTELLUNG(BES_ID),
    PRIMARY KEY (REZ_ID,BES_ID)
);

CREATE TABLE KUNDE_BESCHRAENKUNG_ht (
    KD_ID int(10),
    BES_ID int(10),
    FOREIGN KEY (KD_ID) REFERENCES KUNDE(KD_ID),
    FOREIGN KEY (BES_ID) REFERENCES BESCHRAENKUNG(BES_ID),
    PRIMARY KEY (KD_ID,BES_ID)
);

CREATE TABLE KUNDE_ERNAEHRUNGSKATEGORIE_ht (
    KD_ID int(10),
    ERN_ID int(10),
    FOREIGN KEY (KD_ID) REFERENCES KUNDE(KD_ID),
    FOREIGN KEY (ERN_ID) REFERENCES ERNAEHRUNGSKATEGORIE(ERN_ID),
    PRIMARY KEY (KD_ID,ERN_ID)
);

CREATE TABLE REZEPT_BESCHRAENKUNG_ht (
    REZ_ID int(10),
    BES_ID int(10),
    FOREIGN KEY (REZ_ID) REFERENCES REZEPT(REZ_ID),
    FOREIGN KEY (BES_ID) REFERENCES BESCHRAENKUNG(BES_ID),
    PRIMARY KEY (REZ_ID,BES_ID)
);

CREATE TABLE REZEPT_ERNAEHRUNGSKATEGORIE_ht (
    REZ_ID int(10),
    ERN_ID int(10),
    FOREIGN KEY (REZ_ID) REFERENCES REZEPT(REZ_ID),
    FOREIGN KEY (ERN_ID) REFERENCES ERNAEHRUNGSKATEGORIE(ERN_ID),
    PRIMARY KEY (REZ_ID,ERN_ID)
);

CREATE TABLE REZEPT_ZUTAT_ht (
    REZ_ID int,
    ZUT_ID int,
    MENGE float,
    FOREIGN KEY (REZ_ID) REFERENCES REZEPT(REZ_ID),
    FOREIGN KEY (ZUT_ID) REFERENCES ZUTAT(ZUT_ID),
    PRIMARY KEY (REZ_ID,ZUT_ID)    
);

create table ZUTAT_BESCHRAENKUNG_ht (
    BES_ID int,
    ZUT_ID int,
    FOREIGN KEY (BES_ID) REFERENCES BESCHRAENKUNG(BES_ID),
    FOREIGN KEY (ZUT_ID) REFERENCES ZUTAT(ZUT_ID),
    PRIMARY KEY (BES_ID,ZUT_ID)    
);

CREATE TABLE ZUTAT_ERNAEHRUNGSKATEGORIE_ht (
    ZUT_ID int,
    ERN_ID int,
    FOREIGN KEY (ZUT_ID) REFERENCES ZUTAT (ZUT_ID),
    FOREIGN KEY (ERN_ID) REFERENCES ERNAEHRUNGSKATEGORIE (ERN_ID),
    PRIMARY KEY (ZUT_ID,ERN_ID)
);

